# Hugo New Post

Automate creating new posts for multiple Hugo sites using this bash script. (Tested on mac only)

# Notice

This project has been discontinued ever since I discovered that you can pretty much use Hugo's [Archetypes](https://gohugo.io/content-management/archetypes/) functionality and/ or a slug parameter. 😂