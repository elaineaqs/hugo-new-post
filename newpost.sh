#!/bin/bash
clear

echo "Web directory:";

read webdir;

echo "Post title:";

read newpost;

echo "Post Date Year (YYYY):";

read postyear;

echo "Post Date Month (MM):";

read postmonth;

echo "Post Date Day (DD):";

read postday;

# tags 

options=(
    "Community and Friends"
    "Content Creation and Creativity"
    "Creative Breaks, Rest and Relaxation"
    "Intentions and Mindfulness"
    "Minimalism"
    "Motherhood and Family"
    "Personality and Habits"
    "Podcast"
    "Self Care and Compassion"
    "Self Development and Self Awareness"
    "Slow Entrepreneurship and Flexibility"
    "Vlog")

menu() {
    echo "Avaliable Tag Options:"
    for i in ${!options[@]}; do 
        printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
    done
    if [[ "$msg" ]]; then echo "$msg"; fi
}

prompt="Check an option (again to uncheck, ENTER when done): "
while menu && read -rp "$prompt" num && [[ "$num" ]]; do
    [[ "$num" != *[![:digit:]]* ]] &&
    (( num > 0 && num <= ${#options[@]} )) ||
    { msg="Invalid option: $num"; continue; }
    ((num--)); msg="${options[num]} was ${choices[num]:+un}checked"
    [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
done

printf "You selected"; msg=" "
for i in ${!options[@]}; do 
    [[ "${choices[i]}" ]] && { printf "\n - %s" "${options[i]}"; msg=""; }
done
echo "$msg"


filename="$webdir/$postyear-$postmonth-$postday-$newpost.md";
filename2=${filename// /-};
touch $filename2;


echo "---" >> $filename2;
echo "title: '${newpost}'" >> $filename2;
echo "description: ''" >> $filename2;
echo "date: '${postyear}-${postmonth}-${postday}T09:00:00+08:00'" >> $filename2; 
echo "publishDate: '$postyear-$postmonth-${postday}T09:00:00+08:00'" >> $filename2;
echo "draft: false" >> $filename2;
echo "tags: " >> $filename2;

for i in ${!options[@]}; do 
    [[ "${choices[i]}" ]] && { echo "   - ${options[i]}" >> $filename2; tags=""; }
done

echo "comments: true" >> $filename2;
echo "share: true" >> $filename2;
echo "menu: ''" >> $filename2;
echo "author: ''" >> $filename2;
echo "image: ''" >> $filename2;
echo "slug: ''" >> $filename2;
echo "---" >> $filename2;

echo "Done.";

open "$webdir";
open $filename2;
